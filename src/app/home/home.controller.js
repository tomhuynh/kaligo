(function () {
  'use strict';

  angular
    .module('app')
    .controller('HomeController', HomeController);

  /** @ngInject */
  function HomeController(mockData) {
    var vm = this;

    // Variables
    vm.regexRating = '\\d+';
    vm.sortBy = 'name';

    // Functions
    vm.filterTrustYou = filterTrustYou;

    activate();

    function activate() {
      mockData.get().then(function (response) {
        vm.hotels = response;
      });
    }

    // It's probably beter to create a custom filter.
    function filterTrustYou(hotel) {
      if (!vm.trustyou) {
        return true;
      }
      return parseInt(hotel.trustyou.score.overall) >= parseInt(vm.trustyou) ? hotel : null;
    }
  }
})();
