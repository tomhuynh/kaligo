(function () {
  'use strict';

  angular
    .module('app')
    .service('mockData', mockData);

  function mockData($http) {
    /*
    *	1. Display this json response into a list of hotels, dipslay its name, rating, trustyou overall score (if the hotel has it)
    * 2. Create a Filter select box to filter the hotel by:
    *			- rating
    *			- trustyou overall score
    *	3. Create a text field to filter hotel by name
    * 4. Create a select box to Sort by name / rating / imageCount
    */

    return {
      get: get
    };

    function get() {
      return $http
        .get('services/mockData.json')
        .then(function (response) {
          return response.data;
        });
    }
  }
})();
