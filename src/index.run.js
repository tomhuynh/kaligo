(function () {
  angular
    .module('app')
    .run(runBlock);

  function runBlock() {
    // again, do something when the app is bootstrapped..
  }
})();
