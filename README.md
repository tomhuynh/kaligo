## Please note:
For the sake of time, a few things were not considered:

1. Validating Filter By using RegEx
2. Responsive UI
3. Option to Choose Ascending/Descending Order. You can only sort by Ascending Order.
4. There are no unit/E2E tests.

This application is following John Papa's Styleguide (for the most part)

&

This application was scaffolded using yeoman.

## Instructions

```
$ npm install && bower install
$ gulp serve
```
